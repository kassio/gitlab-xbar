# Gitlab x-bar menu

[x-bar](https://xbarapp.com) menu for GitLab.

![menu](assets/menu.jpg)

## Installation

Link the entry point to your plugins folder:

```shell
BASE_DIRECTORY="${HOME}/src"

git clone https://gitlab.com/kassio/gitlab-xbar.git "${BASE_DIRECTORY}/gitlab-xbar" &&
  ln -sf "${BASE_DIRECTORY}/gitlab-xbar/gitlab.5m.sh" \
    "${HOME}/Library/Application Support/xbar/plugins"

# OR

git clone https://gitlab.com/kassio/gitlab-xbar.git "${BASE_DIRECTORY}/gitlab-xbar" &&
  cd "${BASE_DIRECTORY}/gitlab-xbar" &&
  bin/install
```

Open the configuration and enter your username and access token (read_api scope):

![config](assets/config.jpg)

## Features

- Main menu: GitLab Todo counter
  - Todo submenu:
    - hover: to see a quick list of todos with a distict icon for the todo type (issue, merge request, epic)
    - click: when clicking on the todo submenu it'll open your todo list on your browser
  - Merge Request assigned:
    - hover: to see a quick list of Merge Requests where you're the assignee
    - click: when clicking on the Merge Request submenu it'll open your Assigned Merge Requests dashboard
    - todo [reason](https://docs.gitlab.com/ee/api/graphql/reference/#todoactionenum) emojis:
      - ☎️ => assigned, directly_addressed, mentioned
      - ✅ => marked
      - 💣 => build_failed, merge_train_removed, unmergeable
      - 📝 => approval_required, member_access_requested
      - 🕵️ => review_requested, review_submitted
  - Merge Request reviewer:
    - hover: to see a quick list of Merge Requests where you're a reviwer
    - click: when clicking on the Merge Request submenu it'll open your Review Requests dashboard
  - Issues assigned:
    - hover: to see a quick list of Issues where you're the assignee
    - click: when clicking on the Issues submenu it'll open your Assigned Issues dashboard
  - Issues author:
    - hover: to see a quick list of Issues where you're the author
    - click: when clicking on the Issues submenu it'll open your Authored Issues dashboard
  - Gitlab version:
    - click: when clicking on the Gitlab version submenu it'll open your Gitlab help
  - Refresh:
    - click: when clicking on the Refresh submenu it'll refresh your data

## Contributing

Please, open a Merge Request with your contribution

### Updating icons

There's two important files to manage the icons used in this menu:

- `images.json`: contains the list of images and their sources (remote url)
- `jq/images.json`: contains the images in Base64 format, to be used on xbar.

If a new icon is required, or changed, first update `images.json` to reflect that,
then run `bin/download_images` which will download all the images from `bin/images.json`
to `assets/images` and update the `jq/images.json`.

## Known issues

1. Some pipeline status icons don't have transparency, making them not work properly.
   [Gitlab SVGs issue](https://gitlab.com/gitlab-org/gitlab-svgs/-/issues/389)
