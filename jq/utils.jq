module { "name": "utils" };

def createdAt: .createdAt | fromdate | strflocaltime("%Y-%m-%d %H:%M");

def countPlus:
  if .hasNextPage then
    "\(.count)+"
  elif .count > 0 then
    .count
  else
    ""
  end;

def firstLine:
  . | split("\n") | join(" ") | # join lines
  if . | length >= 180 then     # ensure max of 120chars
    "\(.[:117])..."
  else
    .
  end;

def clean: . |
  gsub("\\|"; "-") |
  firstLine;

# Parse attributes and add defaults (font)
def attributes: { font: "'JetBrains Mono'", size: 13 } + . |
  to_entries |
  map("\(.key)=\(.value)") |
  join(" ");

def title: "\(((.title?) // "" | tostring) | clean) \(. | countPlus) | \(.attrs | attributes)";

def submenuItems:
    (.list?) // [] | map({title: "-- \(.title)", attrs: (.attrs? // {})} | title);

# Returns a xbar submenu
#
# Input format:
#
# {
#    hasNextPage: (bool) indicates if the count is the total of objects
#    count: (int) length of the data set
#    title: (string) menu title
#    attrs: (object) xbar attributes example:
#      - href: (string) menu target url
#      - image: (string) menu image
#      - templateImage: (string) menu image template (alphachannel)
#    list: array of sub-submenu items, with: [{
#      title: (string) menu title
#      attrs: (object) xbar attributes example:
#        - href: (string) menu target url
#        - image: (string) menu image
#        - templateImage: (string) menu image template (alphachannel)
#    }]
# }
#
# Output example:
#
# Todos 3 | href=https://gitlab.com/dashboard/todos templateImage=TODOS_IMG
# -- Some MR Todo | href=https://gitlab.com/group/project/-/merge_requests/1 templateImage=MERGE_REQUEST_IMG
# -- Some Issue Todo | href=https://gitlab.com/group/project/-/issues/1 templateImage=ISSUE_IMG
# ---
def submenu: . | [
  (. | title),
  (. | submenuItems)
] | flatten | join("\n");
