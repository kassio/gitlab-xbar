#!/usr/bin/env zsh

# Metadata allows your plugin to show up in the app, and website.
#
# <xbar.title>GitLab</xbar.title>
# <xbar.version>v0.0.1</xbar.version>
# <xbar.author>Kassio Borges</xbar.author>
# <xbar.author.github>kassio</xbar.author.github>
# <xbar.author.gitlab>kassio</xbar.author.gitlab>
# <xbar.desc>GitLab notification center</xbar.desc>
# <xbar.dependencies>zshell, curl, jq</xbar.dependencies>
# <xbar.abouturl>https://gitlab.com/kassio/gitlab-xbar</xbar.abouturl>
# <xbar.var>string(JQ_PATH="/opt/homebrew/bin"): Where the jq binary can be found.</xbar.var>
# <xbar.var>string(CURL_PATH="/usr/bin"): Where the curl binary can be found.</xbar.var>
# <xbar.var>string(GITLAB_URL="https://gitlab.com"): GitLab base URL.</xbar.var>
# <xbar.var>string(GITLAB_PAGE_SIZE="20"): GitLab `per_page` size (Must be between 1-100).</xbar.var>
# <xbar.var>string(GITLAB_TOKEN=""): GitLab API access token (read_api scope).</xbar.var>
# <xbar.var>string(GITLAB_USERNAME=""): GitLab username.</xbar.var>

[ "${GITLAB_URL}" = "" ] && { echo "GITLAB_URL required"; exit 1 }
[ "${GITLAB_TOKEN}" = "" ] && { echo "GITLAB_TOKEN required"; exit 1 }
[ "${GITLAB_USERNAME}" = "" ] && { echo "GITLAB_USERNAME required"; exit 1 }
([ "${GITLAB_PAGE_SIZE}" -lt 1 ] || [ "${GITLAB_PAGE_SIZE}" -gt 100 ]) &&
  { echo "GITLAB_PAGE_SIZE must be between 1 and 100"; exit 1 }

BASE_DIR="$(dirname "$(realpath ${0})")"

_jq() {
  "${JQ_PATH}/jq" \
    -L "${BASE_DIR}/jq" \
    --arg gitlabURL "${GITLAB_URL}" \
    --arg username "${GITLAB_USERNAME}" \
    --slurpfile images "${BASE_DIR}/jq/images.json" \
    --raw-output \
    --compact-output \
    "$@"
}

graphqlData() {
  query="$(cat "${BASE_DIR}/graphql/$1.graphql" | tr -ds '\n' ' ')"

  _jq \
    --null-input \
    --arg username "${GITLAB_USERNAME}" \
    --arg limit "${GITLAB_PAGE_SIZE}" \
    --arg query "${query}" \
      '{ query: $query, variables: { username: $username, limit: $limit | tonumber } }'
}

_graphql() {
  data="$(graphqlData "$1")"

  {
    "${CURL_PATH}/curl" \
      --silent \
      --location \
      --request "POST" \
      --header "Content-Type: application/json" \
      --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      --url "${GITLAB_URL}/api/graphql" \
      --data-binary "${data}" ||
      echo '{}'; # fallback if the request fails
  } | _jq "$(printf 'include "gitlab"; %s' "$2")"
}

_graphql 'todos' 'todos'
_graphql 'assignedMergeRequests' 'mergeRequests("assignee")'
_graphql 'reviewRequestedMergeRequests' 'mergeRequests("reviewer")'
_graphql 'assignedIssues' 'issues("assignee")'
_graphql 'authoredIssues' 'issues("author")'
_graphql 'gitlabOrgMilestones' 'gitlabOrgMilestones'
_graphql 'gitlabOrgLastMasterPipelineStatus' 'gitlabOrgLatestPipeline'
